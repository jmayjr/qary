# 2020-08-15 DataConLA abstract -- information extraction question answering

Topic: Information extraction with question answering transformers

Summary:

## Abstract
These days, we are all overwhelmed with unstructured text data.
Larger corporations can crowd-source and out-source the data cleaning and information extraction effort.
Individuals and smaller organizations with limited budgets need affordable tools to help them automatically extract vital information from bulk text data.
Fortunately, state of the art Natural Language Processing (NLP) models are now available to all.
In this talk we’ll demonstrate how to use free open source software to extract actionable intelligence from unstructured text data.
You will gain the tools you need to turn natural language text files into relational databases.
You will see how to harness your e-mails, PDF documents, and web pages to power your data-driven decisions.

## take-aways:
* How recent NLP advancements enable businesses to tap into text data
* Use cases for transformers like BERT and GPT-3
* How to extract business-actionable insights from unstructured text
* Practical guide to using free open source software for information extraction



